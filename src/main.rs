#![no_std]
#![no_main]
#![allow(clippy::empty_loop)]

use cortex_m::delay::Delay;
use embedded_graphics::draw_target::DrawTarget;
use embedded_graphics::geometry::Point;
use embedded_graphics::image::{Image, ImageRaw, ImageRawBE};
use embedded_graphics::pixelcolor::{BinaryColor, Rgb565, RgbColor};
use embedded_graphics::primitives::{Circle, Primitive, PrimitiveStyleBuilder};
use embedded_graphics::Drawable;
use embedded_hal::digital::PinState;
use panic_halt as _;
use pimoroni_tufty2040::hal::clocks::{init_clocks_and_plls, ClockSource};
use pimoroni_tufty2040::hal::gpio::{FunctionPio0, PullNone};
use pimoroni_tufty2040::hal::{Clock, Sio, Watchdog};
use pimoroni_tufty2040::pac::{CorePeripherals, Peripherals};
use pimoroni_tufty2040::{entry, DummyPin, Pins, XOSC_CRYSTAL_FREQ};
use st7789::ST7789;

const IMAGE_DATA: &[u8] = include_bytes!("image.png");

#[entry]
fn main() -> ! {
    let mut pac = Peripherals::take().unwrap();
    let cp = CorePeripherals::take().unwrap();

    let mut watchdog = Watchdog::new(pac.WATCHDOG);
    let clocks = init_clocks_and_plls(
        XOSC_CRYSTAL_FREQ,
        pac.XOSC,
        pac.CLOCKS,
        pac.PLL_SYS,
        pac.PLL_USB,
        &mut pac.RESETS,
        &mut watchdog,
    )
    .unwrap();

    let sio = Sio::new(pac.SIO);
    let pins = Pins::new(
        pac.IO_BANK0,
        pac.PADS_BANK0,
        sio.gpio_bank0,
        &mut pac.RESETS,
    );

    // let mut timer = Timer::new(pac.TIMER, &mut pac.RESETS, &clocks);
    let mut delay = Delay::new(cp.SYST, clocks.system_clock.get_freq().to_Hz());

    pins.lcd_backlight
        .into_push_pull_output_in_state(PinState::High);
    pins.lcd_rd.into_push_pull_output_in_state(PinState::High);

    let display_data = {
        use pimoroni_tufty2040::hal::dma::DMAExt;
        use pimoroni_tufty2040::hal::pio::PIOExt;

        let dma = pac.DMA.split(&mut pac.RESETS);
        let (mut pio, sm0, _, _, _) = pac.PIO0.split(&mut pac.RESETS);

        let wr = pins.lcd_wr.reconfigure::<FunctionPio0, PullNone>();
        let d0 = pins.lcd_db0.reconfigure::<FunctionPio0, PullNone>();
        pins.lcd_db1.reconfigure::<FunctionPio0, PullNone>();
        pins.lcd_db2.reconfigure::<FunctionPio0, PullNone>();
        pins.lcd_db3.reconfigure::<FunctionPio0, PullNone>();
        pins.lcd_db4.reconfigure::<FunctionPio0, PullNone>();
        pins.lcd_db5.reconfigure::<FunctionPio0, PullNone>();
        pins.lcd_db6.reconfigure::<FunctionPio0, PullNone>();
        pins.lcd_db7.reconfigure::<FunctionPio0, PullNone>();

        pimoroni_tufty2040::PioDataLines::new(
            &mut pio,
            clocks.system_clock.freq(),
            wr.id(),
            d0.id(),
            sm0,
            dma.ch0,
        )
    };

    let display_interface = pimoroni_tufty2040::ParallelDisplayInterface::new(
        pins.lcd_cs.into_push_pull_output_in_state(PinState::High),
        pins.lcd_dc.into_push_pull_output_in_state(PinState::High),
        display_data,
    );

    let mut display = ST7789::new(display_interface, DummyPin, 240, 320);
    display.init(&mut delay).unwrap();
    display.clear(Rgb565::BLACK).unwrap();

    let mut image_data = [0u8; 320 * 240 * 2];
    for y in 0..320 {
        for x in 0..240 {
            let r = IMAGE_DATA[(y * 240 + x) * 3];
            let g = IMAGE_DATA[(y * 240 + x) * 3 + 1];
            let b = IMAGE_DATA[(y * 240 + x) * 3 + 2];
            let rgb = rgb565::Rgb565::from_rgb888_components(r, g, b);
            let [b1, b2] = rgb.to_rgb565_be();
            image_data[y * 240 + x] = b1;
            image_data[y * 240 + x + 1] = b2;
        }
    }
    let raw_image = ImageRawBE::<Rgb565>::new(&image_data, 240);
    let image = Image::new(&raw_image, Point::zero());
    image.draw(&mut display).unwrap();

    let style = PrimitiveStyleBuilder::default()
        .fill_color(Rgb565::WHITE)
        .build();
    Circle::new(Point::new(50, 50), 20)
        .into_styled(style)
        .draw(&mut display)
        .unwrap();

    loop {}
}
